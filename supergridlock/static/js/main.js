define(["jquery", "backbone", "underscore", "grid", "grid-vis", "cars"],
function($, Backbone, _, Grid, GridVis, Cars) {
	"use strict";

	var SuperGridlock = {};

	SuperGridlock.start_prototype = function() {
		var grid = new Grid();
		var cars = new Cars.Collection();
		cars.spawn(400, grid);
		var gridvis = new GridVis({
			el: $("#roads"),
			model: grid
		});
		gridvis.render(cars);

		cars.each(function (car, i) {
			var tick;
			tick = function() {
				var t = car.tick();
				window.setTimeout(tick, t);
			};
			window.setTimeout(tick, Math.random() * 800);
		}, this);
	};

	return SuperGridlock;
});
