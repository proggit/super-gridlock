define(["underscore", "backbone"],
function(_, Backbone) {
	"use strict";

	var Road = Backbone.Model.extend({
		defaults: {
			maxCapacity: 0,
			capacity: 0,
			a: null,
			b: null
		},
		initialize: function() {
			this.updateCapacity();
		},
		updateCapacity: function() {
			this.set("maxCapacity", this.distance() * 2);
		},
		hasCapacity: function() {
			return (this.get("capacity") < this.get("maxCapacity"));
		},
		distance: function() {
			var a = this.get("a");
			var b = this.get("b");
			return Math.floor(Math.sqrt(Math.pow(a.get("x") - b.get("x"), 2) + Math.pow(a.get("y") - b.get("y"), 2)));
		},
		orientation: function() {
			var a = this.get("a");
			var b = this.get("b");
			if (a.get("x") == b.get("x")) {
				return "h";
			}
			if (a.get("y") == b.get("y")) {
				return "v";
			}
			return null;
		},
		replace: function(prev, curr) {
			var a = this.get("a");
			var b = this.get("b");
			if (a == prev) {
				this.set("a", curr);
			}
			if (b == prev) {
				this.set("b", curr);
			}
			this.updateCapacity();
		},
		otherSideOf: function(item) {
			if (item == this.get("a")) {
				return this.get("b");
			}
			if (item == this.get("b")) {
				return this.get("a");
			}
			return null;
		},
		incr: function() {
			this.set("capacity", this.get("capacity") + 1);
		},
		decr: function() {
			this.set("capacity", this.get("capacity") - 1);
		}
	});

	Road.Collection = Backbone.Collection.extend({
		model: Road
	});

	var Intersection = Backbone.Model.extend({
		defaults: {
			x: null,
			y: null
		},
		initialize: function() {
			this.roads = new Road.Collection();
		},
		requestAccess: function(road) {
			if (road.hasCapacity()) {
				return true;
			}
			return false;
		},
		otherRoad: function(road) {
			if (this.roads.models.length > 1) {
				while (true) {
					var new_road = _.sample(this.roads.models, 1)[0];
					if (new_road != road) {
						return new_road;
					}
				}
			}
			return road;
		}
	});

	Intersection.Collection = Backbone.Collection.extend({
		model: Intersection
	});


	var SquareyGrid = Backbone.Model.extend({
		defaults: {
			width: 20,
			height: 20,
			sparseness: 0.6
		},
		initialize: function() {
			this.intersections = new Intersection.Collection();
			var temp_intersections = new Intersection.Collection();
			this.roads = new Road.Collection();

			var w = this.get("width");
			var h = this.get("height");
			var sparseness = Math.ceil(w * this.get("sparseness"));
			var getRandSparseness = function() {
				return Math.random() < sparseness;
			};
			var min_common = 1;
			var common;

			var addCorners = function(lst) {
				return _.union([1, w], lst);
			};

			var prevSet = null;
			var prevSet_ids = null;
			_.each(_.range(1, h + 1), function(row) {
				var items = _.sample(_.range(1, w + 1), sparseness);
				var item_ids = [];
				var roads;
				if (row == 1 || row == h) {
					items = addCorners(items);
				}
				if (row != 1) {
					// possibly fill in intersectionable intersections
					common = _.intersection(items, prevSet);
					common.sort();
					if (common.length < min_common) {
						items = _.union(_.sample(prevSet, min_common), items);
					}
				}

				items = _.sortBy(items, _.identity);

				// add intersections to collection
				_.each(items, function(v) {
					var i = temp_intersections.add({
						x: v,
						y: row
					});
					item_ids.push(i.cid);
				}, this);

				if (row != 1) {
					// add in roads to collection
					_.each(items, function(v, i) {
						if (i > 0) {
							if (getRandSparseness()) {
								var a = temp_intersections.get(item_ids[i - 1]);
								var b = temp_intersections.get(item_ids[i]);
								var road = this.roads.add({
									a: a,
									b: b
								});
								this.intersections.add([a, b]);
								a.roads.add(road);
								b.roads.add(road);
							}
						}
						if (_.indexOf(common, v) != -1) {
							if (getRandSparseness()) {
								var c = _.indexOf(prevSet, v);
								var a = temp_intersections.get(prevSet_ids[c]);
								var b = temp_intersections.get(item_ids[i]);
								var road = this.roads.add({
									a: a,
									b: b
								});
								this.intersections.add([a, b]);
								a.roads.add(road);
								b.roads.add(road);
							}
						}
					}, this);
				}
				prevSet = items;
				prevSet_ids = item_ids;
			}, this);

			while (true) {
				var c = 0;
				this.intersections.each(function(item) {
					switch (item.roads.length) {
						case 2:
							// clean straightways
							var road_a = item.roads.models[0];
							var road_b = item.roads.models[1];
							if (road_a.orientation() === road_b.orientation()) {
								var new_item = road_b.otherSideOf(item);
								this.intersections.remove(item);
								this.roads.remove(road_b);
								road_a.replace(item, road_b.otherSideOf(item));
								new_item.roads.remove(road_b);
								new_item.roads.add(road_a);
								c = c + 1;
							};
							break;
						case 1:
							// clean dead ends
							var road = item.roads.models[0];
							this.roads.remove(road);
							road.otherSideOf(item).roads.remove(road);
							this.intersections.remove(item);
							c = c + 1;
							break;
					}
				}, this);
				if (c === 0) {
					break;
				}
			};
		}
	});

	_.extend(SquareyGrid.prototype, {
	});

	var Grid = SquareyGrid;

	return Grid;
});
