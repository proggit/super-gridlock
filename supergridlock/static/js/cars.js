define(["underscore", "backbone"],
function(_, Backbone) {

	var STATES = {
		STRAIGHT: 0,
		TURN: 1
	};

	var Cars = Backbone.Model.extend({
		defaults: {
			road: null,
			intersection: null,
			state: STATES.STRAIGHT,
			patience: 0
		},
		initialize: function() {
			var road = this.get("road");
			if (road) {
				road.incr();
			}
		},
		switchRoad: function(new_road, t) {
			var old_road = this.get("road");
			old_road.decr();
			new_road.incr();
			this.set({
				road: new_road,
				state: STATES.STRAIGHT,
				patience: 0
			}, {t: 100});
		},
		tick: function() {
			var item = this.get("intersection");
			var road = this.get("road");

			var t = Math.floor(Math.random() * 500);

			switch (this.get("state")) {
				case STATES.STRAIGHT:
					var new_item = road.otherSideOf(item);
					var d = Math.sqrt(Math.pow(item.get("x") - new_item.get("x"), 2) + Math.pow(item.get("y") - new_item.get("y"), 2));
					t = (t + 1000) * d;
					if (new_item) {
						this.set({
							intersection: new_item,
							state: STATES.TURN,
							patience: 0
						}, {t: t});
					};
					break;
				case STATES.TURN:
					var new_road = item.otherRoad(road);
					t = 100;
					if (new_road) {
						if (item.requestAccess(new_road)) {
							this.switchRoad(new_road);
						} else {
							this.set("patience", this.get("patience") + 1);
						}
					};
					break;
			}
			return t;
		}
	});

	Cars.Collection = Backbone.Collection.extend({
		model: Cars,
		spawn: function(n, grid) {
			_.each(_.range(n), function() {
				var road = _.sample(grid.roads.models, 1)[0];
				var intersection = _.sample([road.get("a"), road.get("b")], 1)[0];
				this.add({
					road: road,
					intersection: intersection
				});
			}, this);
		}
	});

	return Cars;
});
