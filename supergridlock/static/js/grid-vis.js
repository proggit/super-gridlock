define(["jquery", "underscore", "backbone", "d3"],
function($, _, Backbone, d3) {

	var unit = function(v) {
		if (v < 0) {
			return -1;
		}
		if (v > 0) {
			return 1;
		}
		return 0;
	};

	var floored = function(f) {
		return function(v) {
			return Math.floor(f(v));
		};
	};

	var color = function(r, g, b, a) {
		return "rgba(" + r + "," + g + "," + b + "," + a + ")";
	};

	var _Vis = Backbone.View.extend({
		initialize: function(args, opts) {
			this.opts = opts;
			this.listenTo(this.model, "change", this.reDraw);
		}
	});

	var CarVis = _Vis.extend({
		render: function() {
			this.e = this.opts.svg.append("circle");
			var loc = this.model.get("intersection");
			this.e.attr("cx", this.opts.x_axis(loc.get("x")) + "%");
			this.e.attr("cy", this.opts.y_axis(loc.get("y")) + "%");
			this.e.attr("r", 5);
			this.e.style("fill", color(255, 255, 255, 0.4));
		},
		reDraw: function(model, options) {
			var t;
			if (options.t) {
				t = options.t;
			} else {
				t = 500;
			}
			if (model.changed.intersection) {
				var p_loc = this.model.previous("intersection");
				var loc = this.model.get("intersection");
				var x = loc.get("x");
				var y = loc.get("y");
				var px = p_loc.get("x");
				var py = p_loc.get("y");
				this.e
					.transition().duration(100)
						.attr("cx", (this.opts.x_axis(px)) + "%")
						.attr("cy", (this.opts.y_axis(py)) + "%").ease();
				this.e
					.transition().delay(100).duration(t - 100)
						.attr("cx", (this.opts.x_axis(x) + unit(px - x))+ "%")
						.attr("cy", (this.opts.y_axis(y) + unit(py - y))+ "%").ease();
			}

			if (model.changed.road) {
				/*
				var loc = this.model.get("intersection");
				this.e.transition().duration(t)
					.attr("cx", this.opts.x_axis(loc.get("x")) + "%")
					.attr("cy", this.opts.y_axis(loc.get("y")) + "%");
				*/
			}

			if (model.changed.patience) {
				var c = Math.max(0, 255 - model.get("patience") * 50);
				this.e
					.style("fill", color(255, c, c, 0.4));
			}
		}
	});

	var RoadVis = _Vis.extend({
		render: function() {
			this.e = this.opts.svg.append("line");
			this.red_scale = floored(d3.scale.linear().domain([0, this.model.get("maxCapacity")]).range([0, 255]));
			this.alpha_scale = floored(d3.scale.linear().domain([0, this.model.get("maxCapacity")]).range([0, 7]));

			var a = this.model.get("a");
			var b = this.model.get("b");
			this.e.attr("x1", this.opts.x_axis(a.get("x")) + "%");
			this.e.attr("y1", this.opts.y_axis(a.get("y")) + "%");
			this.e.attr("x2", this.opts.x_axis(b.get("x")) + "%");
			this.e.attr("y2", this.opts.y_axis(b.get("y")) + "%");

			this.reDraw();
		},
		reDraw: function() {
			var r = 255 - this.red_scale(this.model.get("capacity"));
			var a = this.alpha_scale(this.model.get("capacity")) / 10;
			var color = "rgba(255, " + r + ", " + r + ", " + (0.3 + a) + ")";
			this.e.attr("stroke", color);
		}
	});

	var IntersectionVis = _Vis.extend({
		render: function() {
			this.e = this.opts.svg.append("rect");

			this.e.attr("y", (this.opts.y_axis(this.model.get("y")) - 1) + "%");
			this.e.attr("x", (this.opts.x_axis(this.model.get("x")) - 1) + "%");
			this.e.attr("width", 2 + "%");
			this.e.attr("height", 2 + "%");

			this.reDraw();
		},
		reDraw: function() {
		}
	});

	var D3Vis = Backbone.View.extend({
		initialize: function() {
			this.$svg = $("#svg");

			var svg = d3.select(this.$svg[0]);

			var x_axis = floored(d3.scale.linear().domain([1, this.model.get("width")]).range([5, 95]));
			var y_axis = floored(d3.scale.linear().domain([1, this.model.get("height")]).range([5, 95]));

			this.opts = {
				svg: svg,
				x_axis: x_axis,
				y_axis: y_axis
			}
		},
		render: function(cars) {
			this.model.roads.each(function(road) {
				var rv = new RoadVis({
					model: road
				}, this.opts);
				rv.render();
			}, this);

			this.model.intersections.each(function(item) {
				var iv = new IntersectionVis({
					model: item
				}, this.opts);
				iv.render();
			}, this);

			cars.each(function(car) {
				var c = new CarVis({
					model: car
				}, this.opts);
				c.render();
			}, this);
		}
	});

	var BadDomVis = Backbone.View.extend({
		initialize: function() {
		},
		render: function() {
			this.$el.empty();
			this.model.intersections.each(function(item) {
				var $e = $("<div>").addClass("intersection");
				$e.css({
					left: item.get("x") * 50,
					top: item.get("y") * 50
				});
				this.$el.append($e);
			}, this);

			this.model.roads.each(function(road) {
				var $e = $("<div>").addClass("road");
				var a = road.get("a");
				var b = road.get("b");
				if (a.get("x") == b.get("x")) {
					$e.css({
						left: a.get("x") * 50 + 2,
						top: a.get("y") * 50,
						width: 8,
						height: (b.get("y") - a.get("y")) * 50
					});
				} else {
					$e.css({
						left: a.get("x") * 50,
						top: a.get("y") * 50 + 2,
						width: (b.get("x") - a.get("x")) * 50,
						height: 8
					});
				}
				this.$el.append($e);
			}, this);
		}
	});

	var GridVis = D3Vis;

	return GridVis;
});
