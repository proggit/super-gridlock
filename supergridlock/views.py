from django.shortcuts import render, redirect

def home(request):
	return redirect("__prototype")

def prototype(request):
	return render(request, "prototype.html")

