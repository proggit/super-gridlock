from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'supergridlock.views.home', name='home'),
    url(r'^prototype$', 'supergridlock.views.prototype', name='__prototype'),
    url(r'^admin/', include(admin.site.urls)),
)
